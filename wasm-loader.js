const WasmModule = {};
const binaryName = 'app';

(function startApp() {
  const canvas = document.querySelector('canvas');
  canvas.width = 800;
  canvas.height = 600;
  WasmModule.canvas = canvas;

  if (window.WebAssembly !== undefined) {
    const request = new XMLHttpRequest();
    request.open('GET', binaryName + '.wasm', true);
    request.responseType = 'arraybuffer';
    request.onload = function() {
      WasmModule.wasmBinary = request.response;
      const script = document.createElement('script');
      script.src = binaryName + '.js';
      document.body.appendChild(script);
    };
    request.send();
  } else {
    throw new Error('WASM not supported');
  }
})();
