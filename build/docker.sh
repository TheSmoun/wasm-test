#!/bin/sh

docker run -it --rm -v $(pwd):/src \
  emscripten/emsdk emcc src/app.cpp -O3 -o app.js -s TOTAL_MEMORY=32mb -s WASM=1 --std=c++11
